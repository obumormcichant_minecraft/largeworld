# Large World

These data and resource packs replace Minecraft’s “Large Biomes” and
“Amplified” world types with “Large World” which scales _most_ noise
parameters horizontally for all dimensions resulting in a world which
retains the same general terrain, continents, and biome arrangement as
a normal world for a given seed but extended farther outwards — 4
times larger (on each of the X and Z axes) for the overworld, 3 times
larger in the nether, and twice as large in the end.

The following images compare a world using the seed -343522682 with
the “Normal” world type versus a “Large World” type.

| Normal | Large World |
| ------ | ----------- |
| ![Normal world](doc/Normal_world.png "Normal world map extending over 2km in each direction") | ![Large world](doc/Large_world-1.png "Large world map showing 2km in each direction") |
| ![Normal basin map](doc/Normal_basin_map.png "Basin in normal world, map view") | ![Large basin map](doc/Large_basin_map.png "Basin in large world, map view") |
| ![Normal basin view](doc/Normal_basin_in-game.png "Basin in normal world, 1st-person view") | ![Large basin view](doc/Large_basin_in-game.png "Basin in large world, 1st-person view") |
| ![Normal cherry grove](doc/Normal_cherry_grove.png "Cherry grove in normal world") |![Large cherry grove](doc/Large_cherry_grove.png "Cherry grove in large world") |

Finer details of the world, including canyons, caves, and tunnels, may
appear in different places but are commesurately longer / wider than
those in a normal world.  The distribution of terrain features and
structures is not changed with this world type, but see the
WidespreadStructures data pack if you want these spread out further
across your large world.

| Normal | Large World |
| ------ | ----------- |
| ![Normal desert](doc/Normal_desert.png "Desert and other warm biomes in normal world") |![Large desert](doc/Large_desert.png "Desert and other warm biomes in large world") |
| ![Normal lush cave](doc/Normal_cave_outside.png "Lush cave in normal world, viewed from over the surface") | ![Large lush cave](doc/Large_cave_outside.png "Lush cave in large world, viewed from over the surface") |
| ![Normal lush cave (inside)](doc/Normal_cave_inside.png "Lush cave in normal world, inside cutaway") | ![Large lush cave (inside)](doc/Large_cave_inside.png "Lush cave in large world, inside cutaway") |

The current build should be compatible with Minecraft Java edition
1.20.2 – 1.21.4 (data pack formats 18–61, resource pack formats 18–46).

## Usage

At the Minecraft welcome screen, click “Options…”, “Resource Packs…”,
and then “Open Pack Folder”.  Copy
`LargeWorld_resource-`_version_`.zip` into this folder, then it will
appear in the list of “Available” resource packs.  Click the rightward
arrow on the resource pack icon to select it, ensuring it appears
_above_ the “Default” Minecraft resources.

Click “Done” and “Done”.  Minecraft should load the new resource pack.

**When creating your world,** go to the “More” tab, click on “Data
Packs”, and “Open Pack Folder”.  Copy
`LargeWorld_data-`_version_`.zip` into this folder, then it will
appear in the list of “Available” data packs.  Click the rightward
arrow on the data pack icon to select it for your world, ensuring it
appears _above_ the “Default” Minecraft data.

Then go to the “World” tab and click on the “World Type” button until
it shows “Large World”.  Enter a seed if you have one you’d like to
use and choose any other desired settings in the “Game” or “More” tab.
When ready, click “Create New World”.  Enjoy your large world!

## Finding Suitable Worlds

[Chunk Base](https://www.chunkbase.com/) has a [Biome
Finder](https://www.chunkbase.com/apps/biome-finder) that can be used
to quickly preview the biome layout of a world.  Set the version to
Java 1.20 and enter any seed or click “Random”.  The layout of a Large
World will be the same except for the coordinates of the biomes which
should be multiplied by 4 for the overworld, 3 for the nether, or 2
for the end (except for the central void, which remains the same
size.)

If you choose to use Chunk Base’ [Seed
Map](https://www.chunkbase.com/apps/seed-map) be aware that features
may _not_ appear in the same places in a Large World as they do in a
normal world, scaled or not.  This is because most features depend on
both the chunk coordinate and the biome or terrain at that chunk.

## Detailed Changes

* All overworld noise settings (e.g. continents, erosion, temperature,
  vegetation, etc.) use ¼ the world X, Z coordinates as a normal world
  to achieve a 4× scale.
* Canyons have a ¹⁄₁₆ reduction in chance to generate per chunk, but
  are 4 times longer and 4 times wider to compensate.
* Caves have a ¹⁄₁₆ reduction in chance to generate per chunk, but are
  4 times wider.
* Nether noise settings (temperature, vegetation) use ⅓ the world X, Z
  coordinates as a normal world to achieve a 3× scale.
* Nether caves have a ¹⁄₉ reduction in chance to generate per chunk,
  but are 3 times wider.
* The End noise settings use ½ the world X, Z coordinates as a normal
  world to achieve a 2× scale.
